//Comprueba en cada uno de los usuarios que tenga al menos dos trimestres aprobados y añade la propiedad ***isApproved***
// a true o false en consecuencia. Una vez lo tengas compruébalo con un ***console.log***.

//Puedes usar este array para probar tu función:

const alumns = [
        {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
		{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
		{name: 'Juan Miranda', T1: false, T2: true, T3: true},
		{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
		{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]


for (let index = 0; index < alumns.length; index++) {
    const element = alumns[index];
    
    var trimestresAprob= 0;
    for(var key in element){
        if((element[key])===true) trimestresAprob= trimestresAprob +1;
    }
    if ( trimestresAprob >=2) alumns[index].isApproved= true;
    else alumns[index].isApproved= false;
}

console.log(alumns);